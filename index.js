console.log("Hello World");

let registeredUsers = [
	
	"James Jeffries",
	"Gunter Smith",
	"Macie West",
	"Michelle Queen",
	"Shane Miguelito",
	"Fernando Dela Cruz",
	"Akiko Yukihime"

];

let friendsList = [];

function register(userName){
	if(registeredUsers.includes(userName)) {
		alert("Registration failed. Username already exists!");
	} else {
		registeredUsers.push(userName);
		alert("Thank you for registering!");
	}
}

function addFriend(userName) {
	if(registeredUsers.includes(userName)) {
		friendsList.push(userName);
		alert("You have added " + userName + " as a friend!");
	} else {
		alert("User not found.");
	}
}

function displayFriends(){
	if(friendsList.length == 0){
		alert("You currently have 0 friends. Add one first.");
	} else {
		friendsList.forEach(function(friend){
			console.log(friend);
		})
	}
}


function displayUsers(){
	if(friendsList.length == 0){
		alert("You currently have 0 friends. Add one first.");
	} else {
		alert("You currently have " + friendsList.length + " friends.")
	}
}

function deleteFriend(){
	if (friendsList.length == 0){
		alert("You currently have 0 friends. Add one first.");

	} else {
		friendsList.pop();
	}
}